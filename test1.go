package main

import "fmt"

func main() {
	var x1, v1, x2, v2 int
	fmt.Scanln(&x1, &v1, &x2, &v2)

	var calMotorOne, calMotorTwo, res int
	for {
		calMotorOne = x1 + v1
		calMotorTwo = x2 + v2

		if calMotorOne == calMotorTwo {
			res = 1
			break
		}

		x1 = calMotorOne
		x2 = calMotorTwo

		if x1 > x2 || x1 > 10000 || x2 > 10000 {
			break
		}
	}

	fmt.Println(res)
}
