package main

import (
	"fmt"
	"math"
	"strconv"
	"strings"
)

func maxCount(str string) float64 {
	data := strings.Split(str, " ")
	dataMap := make(map[int]int)

	for i := 0; i < len(data); i++ {
		n, _ := strconv.Atoi(data[i])
		if _, exists := dataMap[n]; exists {
			dataMap[n] = dataMap[n] + 1
		} else {
			dataMap[n] = 1
		}
	}

	var ans float64

	for key, _ := range dataMap {
		if _, exists := dataMap[key+1]; exists {
			ans = math.Max(ans, float64(dataMap[key])+float64(dataMap[key+1]))
		}
	}

	return ans
}

func main() {
	n := "3 7 6 4 4 2"
	result := maxCount(n)
	fmt.Println("output 1: ", result)

	n = "4 5 5 6 4 5"
	result = maxCount(n)
	fmt.Println("output 2: ", result)
}
