CREATE TABLE tree
(
    node   INT,
    parent INT
)

INSERT INTO tree VALUES
(1,2),
(3,2),
(6,8),
(9,8),
(2,5),
(8,5),
(5,NULL)

SELECT node,
       IF(parent IS NULL, 'R',
          IF(
              (
                  SELECT COUNT(*)
                  FROM tree
                  WHERE parent = t1.node
              ) > 0, 'I', 'L'
      )) AS node_type
FROM tree AS t1
ORDER BY node;
